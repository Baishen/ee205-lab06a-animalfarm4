///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <random>
#include <experimental/random>

#include "animal.hpp"

using namespace std;

namespace animalfarm {
	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
	/// @todo Implement this based on genderName and your work
	///       on Animal Farm 1
   switch (color) {
   case BLACK:    return string("Black"); break;
   case WHITE:    return string("White"); break;
   case RED:      return string("Red"); break;
   case SILVER:   return string("Silver"); break;
   case YELLOW:   return string("Yellow"); break;
   case BROWN:    return string("Brown"); break;
   }

   return string("Unknown");
};
enum Gender Animal::randomGender(){
    int gen = experimental::randint(0, 1);
    cout << "random gender number: " << gen << endl;


    switch(gen) {

case 0: return MALE; break;
case 1: return FEMALE; break;
//case 2: return UNKNOWN; break;
    }

    return UNKNOWN;
}

enum Color Animal::randomColor(){
   int col = experimental::randint(0, 5);
   cout << "random color number: " << col << endl;

   switch(col){
case 0: return BLACK; break;
case 1: return WHITE; break;
case 2: return RED; break;
case 3: return SILVER; break;
case 4: return YELLOW; break;
//case 5: return BROWN; break;
   }
   return BROWN;
}

string Animal::randomName(){
int length = experimental::randint(3, 8);
char array[length];
char C;
char c;
int r;
int R;

cout << "length of name: " << length + 1 << endl;

R = experimental::randint(0, 25);

C = 'A' + R;


array[0] = C;

for(int i = 1; i <= length + 1; i++){

   r = experimental::randint(0, 25);

   c = 'a' + r;

   array[i] = c;   

}


string s = "";
for(int o = 0; o <= length; o++) {
s = s + array[o];
}
return s;


return "Unknown"; 
} 
	
} // namespace animalfarm
